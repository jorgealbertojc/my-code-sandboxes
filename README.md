# my-code-sandboxes

This repository is a compilation of sandboxes that I use to test code that I need to know what's going to do.

In this repository you can find some examples of code from the following languages:

* `c` / `c++`
* `golang`
* `java`
* `javascript`
* `php`
* `python`
* `shellscript`
* `typescript`

Enoy with the examples

## how to contribute

Please, `fork` and create `pull requests`
