package main

import (
	"encoding/json"
	"fmt"
)

type model struct {
	LevelA struct {
		LevelB struct {
			LevelC struct {
				Name string `json:"name"`
			} `json:"level_c"`
		} `json:"level_b"`
	} `json:"level_a"`
}

func printProgress(message string) {
	fmt.Printf("===============: %+v\n", message)
}

func main() {
	printProgress("initializing model")
	mdl := model{}

	printProgress("transforming to structured json")
	body, err := json.MarshalIndent(mdl, "", "    ")
	if err != nil {
		printProgress("an error has ocurred marshalling the model")
		return
	}

	printProgress("validating name")
	if len(mdl.LevelA.LevelB.LevelC.Name) > 0 {
		fmt.Printf("%+v", string(body))
	} else {
		printProgress("the model is empty")
	}
}
