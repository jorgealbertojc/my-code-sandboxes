package main

import "fmt"

func main() {
	var local string
	var unlocal string

	local = "hello"
	fmt.Printf("local: %+v\n\n", local)

	unlocal = local
	fmt.Printf("unlocal: %+v\n\n", unlocal)

	local = "world"
	fmt.Printf("local modified: %+v\n\n", local)
	fmt.Printf("unlocal modified: %+v\n\n", unlocal)
}
