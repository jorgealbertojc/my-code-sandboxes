package service

import "fmt"

// Name description of the interface
type Name interface {
	Create(username string) (Struct, error)
}

// Struct descriptionof the structure
type Struct struct {
	Username string `json:"username,omitempty" yaml:"username,omitempty"`
}

// New instantiation of the service
func New() *Struct {
	return &Struct{}
}

// Create public methods always on top of the code
func (s *Struct) Create() {
	fmt.Printf("welcome to public method: %s", s.Username)
}

// privateMethod always in the final of the code
func privateMethod() {
	fmt.Printf("welcome to private method")
}
