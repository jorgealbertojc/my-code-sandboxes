package request

import (
	"fmt"

	"github.com/parnurzeal/gorequest"
)

func main() {
	req := gorequest.New()
	res, _, errs := req.Get("http://localhost/errors").End()

	if errs != nil {
		fmt.Println(errs)
		return
	}

	fmt.Println(res.StatusCode)
}
