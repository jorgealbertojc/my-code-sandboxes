package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type ArtifactoryAuth struct {
	URL           string `json:"url,omitempty"`
	Username      string `json:"username,omitempty"`
	Password      string `json:"password,omitempty"`
	Policy        string `json:"policy,omitempty"`
	PublicKeyPath string `json:"publicKeyPath,omitempty"`
}

func getStruct() ArtifactoryAuth {
	return ArtifactoryAuth{
		URL:           "http://www.arkosnoemarenom.com",
		Username:      "arkosnoemarenom",
		Password:      "0a8s6df9876asdf69dsaf",
		Policy:        "POLICY PRO",
		PublicKeyPath: "/path/to/public/key.pub",
	}
}

func main() {
	b := getStruct()
	jsonstr, err := json.MarshalIndent(b, "", "    ")
	if err != nil {
		os.Exit(2)
	}
	fmt.Printf("%+v\n", string(jsonstr))
}
