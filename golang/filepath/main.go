package main

import "fmt"

func main() {
	foo()
	defer fmt.Println("GetCurrentFile: ", GetCurrentFile())
	fmt.Println("GetCurrentDir: ", GetCurrentDir())
}
