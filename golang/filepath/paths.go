package main

import (
	"fmt"
	"path"
	"runtime"
)

func foo() {
	_, file, no, ok := runtime.Caller(0)
	if ok {
		// path, _ := filepath.Abs(file)
		fmt.Printf("called from %s:%d\n", file, no-1)
	}
}

// __FILE__
func GetCurrentFile() string {
	_, filename, _, _ := runtime.Caller(1)
	return filename
}

// __DIR__
func GetCurrentDir() string {
	_, filename, _, _ := runtime.Caller(1)
	return path.Dir(filename)
}
