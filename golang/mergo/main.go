package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	listA := []string{"a", "b", "c"}
	listB := []string{"e", "f", "g"}
	listC := []string{}

	listC = append(listC, listA...)
	listC = append(listC, listB...)

	jsonA, _ := json.Marshal(listA)
	fmt.Printf("%+v\n", string(jsonA))

	jsonB, _ := json.Marshal(listB)
	fmt.Printf("%+v\n", string(jsonB))

	jsonC, _ := json.Marshal(listC)
	fmt.Printf("%+v\n", string(jsonC))
}
