package main

import (
	"fmt"
	"strings"
)

func main() {
	mystring := "str one\\nstr two\\nOK...."
	mystring = strings.Replace(mystring, `\n`, "\n", -1)
	mylog := fmt.Sprintf(mystring)
	fmt.Printf("%+v", mylog)
}
