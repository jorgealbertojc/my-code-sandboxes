# golang-sandbox

## Qué no es este repositorio

* _**No es un sandbox real de pruebas online**_

## Que sí es este repositorio

* Es un repositorio en donde se colocan casos de prueba básicos o complejos del lenguaje `GO`
* Espacio de corrección de conceptos
* Repositorio que acepta todas las PR que gusten así como los Issue que sugieran.
* Lugar para agregar sus propios ejemplos
* Repositorio para aportar nuevos ejemplos de `GO` las veces que quieras.
